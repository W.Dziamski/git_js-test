let board = [
	[".", ".", "."],
	[".", ".", "."],
	[".", ".", "."],
];

const setOfWins = [
	[0, 1, 2],
	[3, 4, 5],
	[6, 7, 8],
	[0, 3, 6],
	[1, 4, 7],
	[2, 5, 8],
	[0, 4, 7],
	[2, 4, 6],
];
let didPlayerMoved = false;
let didAiMoved = false;
const buttons = [
	document.querySelector("#button1"),
	document.querySelector("#button2"),
	document.querySelector("#button3"),
	document.querySelector("#button4"),
	document.querySelector("#button5"),
	document.querySelector("#button6"),
	document.querySelector("#button7"),
	document.querySelector("#button8"),
	document.querySelector("#button9"),
];
buttons.forEach((button) => {
	button.addEventListener("click", irlPlayerMove);
});

function randomGenerator(bot, top) {
	const newBot = Math.ceil(bot);
	const newTop = Math.floor(top);
	return Math.floor(Math.random() * (newTop - newBot)) + newBot;
}
function irlPlayerMove(event) {
	if (!didPlayerMoved) {
		console.log(buttons[event]);
		event.target.textContent = "O";
		event.target.disabled = true;
		didPlayerMoved = true;
		didAiMoved = false;
		winCondition();
	}
}

function fakeAiMove() {
	const randomGenConst = randomGenerator(0, buttons.length);
	if (didPlayerMoved && !buttons[randomGenConst].disabled) {
		buttons[randomGenConst].disabled = true;
		buttons[randomGenConst].textContent = "X";
		didPlayerMoved = false;
		didAiMoved = true;
		winCondition();
	}
}

function winConditionCheck() {}

function winCondition() {
	if (
		(buttons[0].innerText === "X" &&
			buttons[1].innerText === "X" &&
			buttons[2].innerText === "X") ||
		(buttons[3].innerText === "X" &&
			buttons[4].innerText === "X" &&
			buttons[5].innerText === "X") ||
		(buttons[6].innerText === "X" &&
			buttons[7].innerText === "X" &&
			buttons[8].innerText === "X") ||
		(buttons[0].innerText === "X" &&
			buttons[3].innerText === "X" &&
			buttons[6].innerText === "X") ||
		(buttons[1].innerText === "X" &&
			buttons[4].innerText === "X" &&
			buttons[7].innerText === "X") ||
		(buttons[2].innerText === "X" &&
			buttons[5].innerText === "X" &&
			buttons[8].innerText === "X") ||
		(buttons[0].innerText === "X" &&
			buttons[4].innerText === "X" &&
			buttons[8].innerText === "X") ||
		(buttons[2].innerText === "X" &&
			buttons[4].innerText === "X" &&
			buttons[6].innerText === "X")
	) {
		console.log("input win for x");
		alert("X wins");
		return "X";
	}
	if (
		(buttons[0].innerText === "O" &&
			buttons[1].innerText === "O" &&
			buttons[2].innerText === "O") ||
		(buttons[3].innerText === "O" &&
			buttons[4].innerText === "O" &&
			buttons[5].innerText === "O") ||
		(buttons[6].innerText === "O" &&
			buttons[7].innerText === "O" &&
			buttons[8].innerText === "O") ||
		(buttons[0].innerText === "O" &&
			buttons[3].innerText === "O" &&
			buttons[6].innerText === "O") ||
		(buttons[1].innerText === "O" &&
			buttons[4].innerText === "O" &&
			buttons[7].innerText === "O") ||
		(buttons[2].innerText === "O" &&
			buttons[5].innerText === "O" &&
			buttons[8].innerText === "O") ||
		(buttons[0].innerText === "O" &&
			buttons[4].innerText === "O" &&
			buttons[8].innerText === "O") ||
		(buttons[2].innerText === "O" &&
			buttons[4].innerText === "O" &&
			buttons[6].innerText === "O")
	) {
		console.log("input win for O");
		alert("O wins");
		return "O";
	}
	return false;
}




function optimalWinConditions(){

//edit do pulla
	
}